var io = require('socket.io')();

var players = [];

io.on('connection', function(socket){
  socket.on('lobbyJoin', function(name){
    if (players.find(player => player.name === name)) {
        return socket.disconnect();
    }

    players.push({ name, socket });

    console.log(`${name} just connected`);
  });
  socket.on('disconnect', function(){
    var index = players.findIndex(player => player.socket.id === socket.id);

    if (index === -1) {
      return;
    }

    var name = players[index].name;

    players.splice(index, 1);
    io.emit('playerLeave', name);

    console.log(`${name} just disconnected`);
  });
  socket.on('message', function(msg){
    io.emit('message', msg);
  });
  socket.on('removeMessage', function(msg){
    io.emit('removeMessage', msg);
  });
});

const port = 4000;
io.listen(port);
console.log('listening on port ', port);
