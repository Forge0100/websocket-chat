import React, { useReducer } from 'react';
import { Switch, Route } from "react-router-dom";
import { Context, initialState, reducer } from "./store";
import Home from './containers/Home';
import Login from './containers/Login';

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <Context.Provider value={{ state, dispatch }}>
      <Switch>
        <Route path='/login'>
          <Login />
        </Route>
        <Route path='/'>
          <Home />
        </Route>
      </Switch>
    </Context.Provider>
  );
}

export default App;
