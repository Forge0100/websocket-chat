export const initialState = {
  profile: {
    name: null,
    color: null
  },
  messages: []
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'setProfile':
      return { ...state, profile: action.payload };
    case 'removeProfile':
      return { ...state, profile: initialState.profile };
    case 'addMessage': {
      const { name, message } = action.payload;

      return {
        ...state,
        messages: [
          ...state.messages,
          { name, message }
        ],
      };
    }
    case 'removeMessage':
      return {
        ...state,
        messages: state.messages.filter((_, key) => key !== action.payload)
      };
    default:
      return state;
  }
};
