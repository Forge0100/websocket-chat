import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useStore } from '../store';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = () => {
  const classes = useStyles();
  const history = useHistory();
  const { dispatch } = useStore();
  const [values, setValues] = useState({
    name: '',
    color: ''
  });
  const { name, color } = values;

  const handleChange = (event) => {
    const { target } = event;
    const { name, value } = target;

    event.persist();

    setValues({ ...values, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch({ type: 'setProfile', payload: values });
    history.push("/");
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Nickname"
            name="name"
            autoFocus
            value={name}
            onChange={handleChange}
          />
          <FormControl variant="outlined" className={classes.formControl} fullWidth required>
            <InputLabel id="demo-simple-select-outlined-label">Color</InputLabel>
            <Select
              labelId="color"
              id="color"
              name="color"
              value={color}
              onChange={handleChange}
              label="Color"
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value="#f44336">Red</MenuItem>
              <MenuItem value="#2196f3">Blue</MenuItem>
              <MenuItem value="#009688">Green</MenuItem>
            </Select>
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Enter chat
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default Login;
