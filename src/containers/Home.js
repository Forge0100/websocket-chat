import React, {useCallback, useEffect, useRef, useState} from "react";
import { useHistory } from 'react-router-dom';
import socketIOClient from "socket.io-client";
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from "../store";

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
  chat: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    flex: 1,
    overflow: 'auto',
  },
  message: {
    position: 'relative',
    background: theme.palette.text.primary,
    color: '#fff',
    margin: '10px',
    padding: '8px',
    borderRadius: '8px',
    fontSize: '14px',
  },
  me: {
    background: props => props.color,
    alignSelf: 'flex-end',
  },
  username: {
    fontSize: '12px',
  },
  closeButton: {
    position: 'absolute',
    top: '-5px',
    right: '-5px',
    backgroundColor: '#fff',
    color: '#000',
    borderRadius: '20px',
    width: '15px',
    height: '15px',
    fontSize: '9px',
    fontStyle: 'initial',
    textAlign: 'center',
    cursor: 'pointer',
  },
  messageInput: {
    padding: '4px'
  },
  submit: {
    color: props => props.color,
  },
}));

const Home = () => {
  const history = useHistory();
  const socketRef = useRef();
  const { state, dispatch } = useStore();
  const [message, setMessage] = useState('');
  const { profile, messages } = state;
  const classes = useStyles({ color: profile.color });

  if (!profile.name) {
    history.push('/login');
  }

  const messageClassNames = (name) => classNames(classes.message, { [classes.me]: profile.name === name });

  const handleExit = useCallback(() => {
    dispatch({ type: 'removeProfile' });
    history.push('/login');
  }, [dispatch, history]);

  const handleRemove = index => () => {
    socketRef.current.emit('removeMessage', index);
  };

  const handleChange = useCallback((event) => {
    setMessage(event.target.value);
  }, [setMessage]);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!message) {
      return;
    }

    socketRef.current.emit('message', { name: profile.name, message });
    setMessage('');
  };

  useEffect(() => {
    socketRef.current = socketIOClient('http://127.0.0.1:4000');
    socketRef.current.on('connect', () => {
      socketRef.current.emit('lobbyJoin', profile.name);
    });
    socketRef.current.on('disconnect', () => {
      history.push('/login');
    });
    socketRef.current.on('playerLeave', (name) => {
      console.log(`${name} just disconnected`);
    });
    socketRef.current.on('message', (message) => {
      dispatch({type: 'addMessage', payload: message});
    });
    socketRef.current.on('removeMessage', (index) => {
      dispatch({ type: 'removeMessage', payload: index });
    });

    return () => {
      socketRef.current.disconnect();
    }
  }, [history, dispatch, profile.name]);

  return (
    <Grid container className={classes.root} direction="column">
      <Grid item xs className={classes.chat}>
        {messages.map(({ message, name }, index) => (
          <div key={index} className={messageClassNames(name)}>
            {profile.name === name && (
              <i
                className={classes.closeButton}
                onClick={handleRemove(index)}
              >x</i>
            )}
            {profile.name !== name && (
              <div className={classes.username}>
                {name}
              </div>
            )}
            {message}
          </div>
        ))}
      </Grid>
      <Grid
        component="form"
        direction="row"
        container
        item
        alignItems="center"
        onSubmit={handleSubmit}
      >
        <Grid item xs>
          <TextField
            value={message}
            onChange={handleChange}
            fullWidth
            autoFocus
            placeholder="Message..."
            InputProps={{
              disableUnderline: true,
              className: classes.messageInput,
            }}
          />
        </Grid>
        <Grid item>
          <Button
            type="submit"
            className={classes.submit}
          >
            Send
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={handleExit}
          >
            Exit
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Home;
